// FFICoProcess.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

#include<windows.h>
#include <stdio.h>

extern "C" __declspec(dllexport) const char* GetLastErrorMessage(void);
extern "C" __declspec(dllexport) HANDLE LaunchProcess(char* commandLine);
extern "C" __declspec(dllexport) bool IsPipeEmpty(HANDLE hOutputRead);
extern "C" __declspec(dllexport) int ReadProcessOutputInto(HANDLE hOutputRead, char* buffer, int size);
extern "C" __declspec(dllexport) bool CloseOutputPipe(HANDLE hOutputRead);


CHAR szErrorMessageBuffer[512];

const char* GetLastErrorMessage(void) {
	return szErrorMessageBuffer;
}

void FormatError(const char* apiName) {

	LPVOID lpvMessageBuffer;

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, GetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpvMessageBuffer, 0, NULL);

	sprintf_s(szErrorMessageBuffer,
		"ERROR: API    = %s.\n   error code = %d.\n   message    = %s.\n",
		apiName, GetLastError(), (char *)lpvMessageBuffer);

	LocalFree(lpvMessageBuffer);
}

HANDLE LaunchProcess(char* commandLine)
{
	HANDLE hOutputReadTmp, hOutputRead, hOutputWrite;
	HANDLE hErrorWrite;
	SECURITY_ATTRIBUTES sa;

	// Set up the security attributes struct.
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;

	// Create the child output pipe.
	if (!CreatePipe(&hOutputReadTmp, &hOutputWrite, &sa, 0)) {
		FormatError("CreatePipe");
		return NULL;
	}

	// Create a duplicate of the output write handle for the std error
	// write handle. This is necessary in case the child application
	// closes one of its std output handles.
	if (!DuplicateHandle(GetCurrentProcess(), hOutputWrite,
		GetCurrentProcess(), &hErrorWrite, 0,
		TRUE, DUPLICATE_SAME_ACCESS)) {
		FormatError("DuplicateHandle");
		return NULL;
	}
	// Create new output read handle. Set
		  // the Properties to FALSE. Otherwise, the child inherits the
		  // properties and, as a result, non-closeable handles to the pipes
		  // are created.

	if (!DuplicateHandle(GetCurrentProcess(), hOutputReadTmp,
		GetCurrentProcess(),
		&hOutputRead, // Address of new handle.
		0, FALSE, // Make it uninheritable.
		DUPLICATE_SAME_ACCESS)) {
		FormatError("DupliateHandle");
		return NULL;
	}
	if (!CloseHandle(hOutputReadTmp)) {
		FormatError("CloseHandle");
		return NULL;
	}
	// create process

	PROCESS_INFORMATION pi;
	STARTUPINFOA si;

	// Set up the start up info struct.
	ZeroMemory(&si, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
	si.hStdOutput = hOutputWrite;
	si.hStdInput = NULL;
	si.hStdError = hErrorWrite;

	// Use this if you want to hide the child:
	si.wShowWindow = SW_HIDE;
	// Note that dwFlags must include STARTF_USESHOWWINDOW if you want to
	// use the wShowWindow flags.

	// Launch the process that you want to redirect (in this case,
	// Child.exe). Make sure Child.exe is in the same directory as
	// redirect.c launch redirect from a command line to prevent location
	// confusion.

	if (!CreateProcessA(NULL, commandLine, NULL, NULL, TRUE,
		CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi)) {
		FormatError("CreateProcess");
		return NULL;
	}

	// Close any unnecessary handles.
	if (!CloseHandle(pi.hThread)) {
		FormatError("CloseHandle");
		return NULL;
	}

	// Close pipe handles (do not continue to modify the parent).
	 // You need to make sure that no handles to the write end of the
	 // output pipe are maintained in this process or else the pipe will
	 // not close when the child process exits and the ReadFile will hang.
	if (!CloseHandle(hOutputWrite))	{
		FormatError("CloseHandle");
		return NULL;
	}
	if (!CloseHandle(hErrorWrite)) {
		FormatError("CloseHandle");
		return NULL;
	}

	return hOutputRead;
}

bool IsPipeEmpty(HANDLE hOutputRead) {

	DWORD amountAvailable = 0;

	return PeekNamedPipe(hOutputRead,
		NULL,
		0,
		NULL,
		&amountAvailable,
		NULL
	) != 0 && amountAvailable == 0;
}

int ReadProcessOutputInto(HANDLE hOutputRead, char* buffer, int size) {

	DWORD nBytesRead = 0;

	if (!ReadFile(hOutputRead, buffer, size,
			&nBytesRead, NULL) || nBytesRead == 0)
		{
			if (GetLastError() == ERROR_BROKEN_PIPE)
				return nBytesRead; // pipe done - normal exit path.
			else
			{
				FormatError("ReadFile"); // Something bad happened.
				return -1;
			}
		}
	return nBytesRead;
}

bool CloseOutputPipe(HANDLE hOutputRead) 
{
	if (!CloseHandle(hOutputRead)) {
		FormatError("CloseHandle");
		return FALSE;
	}
	return TRUE;
}
